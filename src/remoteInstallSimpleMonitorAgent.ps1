param(
    $machinesProfilesDir = "..\profiles",
    [switch] $uninstall,
    [switch] $version,
    [switch] $mockInvoke
)

$scriptPath = Split-Path -Parent $MyInvocation.MyCommand.Definition
if ($version) {
    Get-Content "$scriptPath\version" -Encoding UTF8
    exit 0
}

$agent = @{
    machinesProfilesDir = $machinesProfilesDir;
    scriptDir = $scriptPath;
    profiles = @{}
}

function Message($message, $color = "WHITE", $level = "INFO") {
    Write-Host "[$(Get-Date -Format "yyyy-MM-dd HH:mm:ss.fff")][$(hostname)][$level] $message" -ForegroundColor $color
}

function Error($message, $exitCode) {
    Message $message "RED" "ERROR"
    Write-Host $([string](Get-PSCallStack) -replace ",","`r`n")
    exit $exitCode
}

function CheckParams () {
    Message "Checking script's params"
    if ([string]::IsNullOrWhiteSpace($machinesProfilesDir)) {
        Error "machinesProfilesDir cannot be empty" 1
    }
    if (!(Test-Path ($agent.machinesProfilesDir))) {
        Error "Profiles dir [$($agent.machinesProfilesDir)] does not exits or is not accesible" 2
    }
    Message "Checking script's params - finished"
}

function GetAllProfiles() {
    Message "Reading all profiles in [$($agent.machinesProfilesDir)]"
    $profiles = @(Get-ChildItem $agent.machinesProfilesDir -ErrorAction SilentlyContinue | 
        where { !$_.PSIsContainer -and $_.FullName.EndsWith(".xml") })
    if ($profiles.Length -eq 0) {
        Error "No profiles found in specified path" 3
    }
    Message "Found profiles: $([string]::Join(", ", $profiles))"
    foreach($xmlFile in $profiles) {
        Message "Loading profile: $($xmlFile.FullName)"
        $xmlFileContent = [xml](Get-Content $xmlFile.FullName -Encoding UTF8)
        $agent.profiles.Add($xmlFile, $xmlFileContent)
    }
    Message "Reading all profiles - finished"
}

function CopyAgentFiles($xmlProfile, $xmlFilePath) {
    $destinyShare = ("\\{0}\{1}" -f `
        $xmlProfile.simpleMonitorAgent.agentHost, `
        ($xmlProfile.simpleMonitorAgent.agentDir -replace "([a-zA-Z]):",'$1$$'))
    Message "Copying agent files from $($agent.scriptDir) to [$($destinyShare)]"
    if(Test-Path $destinyShare) {
        Message "Removing old files" "Yellow"
        Remove-Item "$destinyShare\*" -Force -Recurse -Verbose
        Message "Old files removed" "Yellow"
    }
    Copy-Item -Path ("$($agent.scriptDir)\*") -Destination $destinyShare -Force -Recurse -Verbose -ErrorAction SilentlyContinue
    if (!$?) {
        Message "Error during copy to [$($destinyShare)]. Remote install for [$($agent.agentHost)] aborted." "RED" "ERROR"
        return $false
    }
    Message "Copying agent profile [$($xmlFilePath)]"
    Copy-Item -Path $xmlFilePath -Destination ($xmlProfile.simpleMonitorAgent.agentDir)  -Force -Verbose
    Message "Files are copiedy to destination"
    return $true
}

function RunRemoteScriptInstall($xmlProfile, $xmlFilename) {
    Message "Running remote script install on $($xmlProfile.simpleMonitorAgent.agentHost)"
    $installScript = [System.IO.Path]::Combine($($xmlProfile.simpleMonitorAgent.agentDir), "installSimpleMonitorAgent.ps1")
    Message ("Installation will be performed on [{0}]. Installation script remote path [{1}]" -f $xmlProfile.simpleMonitorAgent.agentHost, $installScript)
    if ($mockInvoke.IsPresent) {
        Message "Mock version - Invoke-Command does not work on my PC :(" "YELLOW"
        &$installScript -profileFile $xmlFilename
        Message "Installation fnished"
    } else {
        Invoke-Command -ComputerName ($xmlProfile.simpleMonitorAgent.agentHost) -ScriptBlock {
            param($installScript, $xmlFilename)
            Write-Host "Hello from $(hostname)"
            &$installScript -profileFile $xmlFilename
            Write-Host "Installation on $(hostname) - finished"
        } -ArgumentList $installScript, $xmlFilename
    }
    Message "Running remote script install - finished"
}

function RunRemoteScriptUninstall($xmlProfile, $xmlFilename) {
    Message "Running remote script uninstall on $($xmlProfile.simpleMonitorAgent.agentHost)"
    $installScript = [System.IO.Path]::Combine($($xmlProfile.simpleMonitorAgent.agentDir), "installSimpleMonitorAgent.ps1")
    Message ("Uninstallation will be performed on [{0}]. Installation script remote path [{1}]" -f $xmlProfile.simpleMonitorAgent.agentHost, $installScript)
    if ($mockInvoke.IsPresent) {
        Message "Mock version - Invoke-Command does not work on my PC :(" "YELLOW"
        &$installScript -uninstall
        Message "Uninstallation fnished"
    } else {
        Invoke-Command -ComputerName ($xmlProfile.simpleMonitorAgent.agentHost) -ScriptBlock {
            param($installScript, $xmlFilename)
            Write-Host "Hello from $(hostname)"
            &$installScript -uninstall
            Write-Host "Uninstallation on $(hostname) - finished"
        } -ArgumentList $installScript, $xmlFilename
    }
    Message "Running remote script uninstall - finished"
}

function UninstallOnRemotes() {
    Message "Running uninstall on remotes"
    foreach ($xmlFilename in $agent.profiles.Keys) {
        Message "Processing $xmlFilename"
        RunRemoteScriptUninstall ($agent.profiles[$xmlFilename]) $xmlFilename
    }
    Message "Running uninstall on remotes - finished"
}

function DeployProfiles() {
    Message "Deploying profiles"
    foreach ($xmlFilename in $agent.profiles.Keys) {
        Message "Processing $xmlFilename"
        $copyResult = CopyAgentFiles $agent.profiles[$xmlFilename] ([System.IO.Path]::Combine($agent.machinesProfilesDir, $xmlFilename))
        if($copyResult) {
            RunRemoteScriptInstall ($agent.profiles[$xmlFilename]) $xmlFilename
        } else {
            Message "Deploying profile failed on $($agent.profiles[$xmlFilename])" "RED" "ERROR"
        }
    }
    Message "Deploying profile - finished"
}

function Run () {
    Message "Running remote SimpleMonitorAgent installation"
    CheckParams
    GetAllProfiles
    if ($uninstall.IsPresent) {
        UninstallOnRemotes
    } else {
        DeployProfiles
    }

    Message "Running remote SimpleMonitorAgent installation - finished"
}

Run