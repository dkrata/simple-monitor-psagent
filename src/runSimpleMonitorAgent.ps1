param (
    [string] $agentProfilePath = "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)\profiles\agentProfile.xml",
    [string] $logPath = "",
    [switch] $version
)

$scriptPath = Split-Path -Parent $MyInvocation.MyCommand.Definition

if ($version) {
    Get-Content "$scriptPath\version" -Encoding UTF8
    exit 0
}

$agent = @{}
$agent.profilePath = $agentProfilePath
$agent.logPath = $logPath
$agent.hostname = hostname
$agent.path = $scriptPath
$agent.result = @{}
$agent.outputFilename = "$(hostname).json"
$agent.outputFilePath = "$($agent.path)\$($agent.outputFilename)"
$agent.modules = @{}
$agent.modules.path = "$($agent.path)\modules"

function Message($message, [boolean] $isError = $false) {
    $color = "WHITE"
    $level = "INFO"
    if ($isError) {
        $color = "RED"
        $level = "ERROR"
    }
    $log = "[$(Get-Date -Format "yyyy-MM-dd HH:mm:ss.fff")][$(hostname)][$level] $message"
    Write-Host $log -ForegroundColor $color
    Log $log
}


function Error($message, $exitCode) {
    Message $message $true
    Write-Host $([string](Get-PSCallStack) -replace ",","`r`n")
    exit $exitCode
}

function Log($message) {
    if ([string]::IsNullOrWhiteSpace($agent.logPath)) {
        $agent.logPath = "$($agent.path)\SimpleMonitorAgent.log"
        Message "Setting default log path: $($agent.logPath)"
    }
    $message | Out-File ($agent.logPath) -Append -Encoding UTF8 -Force
}

function LoadModules() {
    Message "Loading SimpleMonitorAgent modules in [$($agent.modules.path)]"
    $modules = @(Get-ChildItem "$($agent.modules.path)\*.ps1" | % { $_.FullName })
    if($modules.Length -eq 0) {
        Message "No modules found!"
        return
    }
    foreach($modulePath in $modules) {
        $moduleName = [System.IO.Path]::GetFileNameWithoutExtension($modulePath)
        $agent.modules.$moduleName = $modulePath
    }
    Message "Modules are loaded: `r`n$([string]::Join("`r`n",$modules))"
}
function CheckParams() {
    Message "Checking script's params"
    if ([string]::IsNullOrWhiteSpace($agent.profilePath)) {
        Error "Parameter agentProfilePath is empty" 1
    }

    $item = Get-Item ($agent.profilePath) -ErrorAction SilentlyContinue
    if(!$item) {
        Error "Path [$($agent.profilePath)] does not exists" 2
    }

    if ($item.PSIsContainer) {
        Error "Object [$($agent.profilePath)] is not a file" 3
    }

    Message "Script parameter are checked"
}

function CopyReport($reportPath) {
    Message "Copying report [$($agent.outputFilePath)] to [$reportPath]"
    $dir = Get-Item $reportPath -ErrorAction SilentlyContinue
    if (!$dir) {
        Message "Destination directory does not exists. Creating directory..."
        New-Item $reportPath -ItemType Directory -Force | Out-Null
        Message "Destination directory created"
}
    Copy-Item $agent.outputFilePath -Destination "$reportPath$($agent.outputFilename)" -Force
    Message "Copying report is finished"
}

function GetParams($xmlElement) {
    $params = @{}
    foreach($argument in $xmlElement.ChildNodes) {
        Message ("Param - Name: {0}, Value: {1}" -f $argument.Name, $argument.InnerText)
        $params.$($argument.Name) = $argument.InnerText
}
    return $params
    }

function GetScriptBlock($scriptPath, $scriptParams) {
    return [scriptblock]::create(".{$(get-content $($scriptPath) -Raw)} $(&{$args} @scriptParams)")
                }

function RunScriptBlock($scriptBlock, $jobName) {
    Message "Running $jobName"
    $result = Invoke-Command -ScriptBlock $scriptBlock
    Message "Script finished"
    return $result
                }

function RunJobs($jobs) {
    $jobCount = 0
    $jobNumber = $childNodes.Length
    $jobList = $childNodes | ForEach-Object { $_.Name }
    Message "Running $jobNumber jobs. Jobs list: $([string]::Join(",", $jobList))"
    foreach($job in $childNodes) {
        $jobCount++
        Message "Job $jobCount of $jobNumber`: $($job.Name), Multiple items: $($job.multipleItems), List name: [$($job.listName)], Item Name: [$($job.itemName)]"

        if($job.multipleItems) {
            $list = @()
            foreach($item in $job.ChildNodes) {
                $params = GetParams $item
                $scriptBlock = GetScriptBlock $agent.modules.($job.Name) $params
                $list += RunScriptBlock $scriptBlock $job.Name
            }
            $agent.result.$($job.listName) = $list
            Message "Result for $($job.Name) added"
        } else {
            $params = GetParams $item
            $scriptBlock = GetScriptBlock $agent.modules.($job.Name) $params
            $agent.result.$($job.itemName) = RunScriptBlock $scriptBlock $job.Name
}
        Message "Job finished"
    }
    Message "All jobs finished"
}

function RunAgent() {
    Message "Simple Monitor Agent is running"
    LoadModules
    CheckParams

    Message "Getting profile xml from [$($agent.profilePath)]"
    $xmlProfile = [xml] (Get-Content $agent.profilePath -Encoding "UTF8")

    $childNodes = $xmlProfile.simpleMonitorAgent.ChildNodes | Where-Object { $_.NodeType -notlike "Comment" }
    RunJobs $childNodes

    Message "Saving JSON do file [$($agent.outputFilePath)]"
    $agent.result | ConvertTo-Json | Out-File $agent.outputFilePath
    CopyReport $xmlProfile.simpleMonitorAgent.reportDestinyPath

    Message "Simple Monitor Agent finished work`r`n"
}

RunAgent
exit 0