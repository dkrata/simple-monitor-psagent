<#
.SYNOPSIS
    Script is looking for desired service and returns service details.
.DESCRIPTION
    Script is looking for desired service and returns service details.
.EXAMPLE
    PS C:\> getServiceDetails -serviceName eventlog
    Script is looking for eventlog service details.
.INPUTS
    serviceName - name of service to find
.OUTPUTS
    Servic details result or result with error described in status (multiple results / not found)
#>
param(
    $serviceName
)
Write-Host "Looking for: $serviceName"
$services = @(Get-WmiObject -Class Win32_service -Filter "name LIKE '$($serviceName)%'")
Write-Host "Found services: " $services.Length

if ($services.Length -gt 1) {
    Write-Host "Name of service is ambigious - found more than 1 service"
    return @{
        name = "$serviceName";
        username = "_UNKNOWN_";
        state = "_UNKNOWN_";
        status = "MULTIPLE RESULTS";
        statusDateTime = (Get-Date -f "yyy-MM-dd HH:mm:ss");
    }
}

if ($services.Length -eq 0) {
    Write-Host "Service is not found"
    return @{
        name = "$serviceName";
        username = "_UNKNOWN_";
        state = "_UNKNOWN_";
        status = "NOT FOUND";
        statusDateTime = (Get-Date -f "yyy-MM-dd HH:mm:ss");
    }
}

return @{
    name = $services[0].Name;
    username = $services[0].StartName;
    state = $services[0].State;
    status = $services[0].Status;
    statusDateTime = (Get-Date -f "yyy-MM-dd HH:mm:ss");
}