param(
    $letter,
    $warning,
    $critical
)
if (-Not $warning) {
    Write-Host "Setting default threshold of warning at 10%"
    $warning = 10
}

if (-Not $critical) {
    Write-Host "Setting default threshold of critical at 5%"
    $critical = 5
}

$drive = (Get-PSDrive -PSProvider FileSystem -Name $letter)
$driveSize = [Math]::Round(($drive.Used + $drive.Free) / 1MB, 2)
$driveFreeSpace = [Math]::Round($drive.Free / 1MB, 2)
$statusDateTime = (Get-Date -f "yyy-MM-dd HH:mm:ss")
$freeSpacePercentage = $driveFreeSpace / $driveSize * 100

$driveStatus = "healthy"
if ($freeSpacePercentage -lt $critical) {
    $driveStatus = "critical"
} elseif ($freeSpacePercentage -lt $warning) {
    $driveStatus = "warning"
}

return @{
    letter = $letter
    size = $driveSize;
    freeSpace = $driveFreeSpace;
    statusDateTime = $statusDateTime;
    status = $driveStatus;
}