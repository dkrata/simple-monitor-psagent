$upTime = ((get-date) -(gcim Win32_OperatingSystem).LastBootUpTime)

return @{
    days = $upTime.Days;
    hours = $upTime.Hours;
    minutes = $upTime.Minutes;
    seconds = $upTime.Seconds;
    statusDateTime = (Get-Date -f "yyy-MM-dd HH:mm:ss")
}