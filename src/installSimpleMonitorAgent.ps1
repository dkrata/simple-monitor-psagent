# As administrator
# I want to install simple agent remotely automatically
# Because I want a fast and reliable way to install agent on multiple machines
# * Install script should create a task on remote machine
# * task should run on every X minutes described in a host profile file

param(
    $destinationDir = "D:\SimpleMonitorAgent\",
    $profileFile = "agentProfile.xml",
    $account = "",
    $password = "",
    [switch] $uninstall,
    [switch] $version
)

$scriptPath = Split-Path -Parent $MyInvocation.MyCommand.Definition
if ($version) {
    Get-Content "$scriptPath\version" -Encoding UTF8
    exit 0
}

$agent = @{
    taskName = "SimpleMonitorAgent";
    profileFile = $profileFile;
    destinationDir = $destinationDir;
    scriptDir = (Split-Path $script:MyInvocation.MyCommand.Path);
    account = $account;
    taskPath = "SimpleMonitorAgent";
}

function Message($message, $color = "WHITE", $level = "INFO") {
    Write-Host "[$(Get-Date -Format "yyyy-MM-dd HH:mm:ss.fff")][$(hostname)][$level] $message" -ForegroundColor $color
}

function Error($message, $exitCode) {
    Message $message "RED" "ERROR"
    Write-Host $([string](Get-PSCallStack) -replace ",","`r`n")
    exit $exitCode
}

function CheckParams() {
    Message "Checking script's params"
    $systemAccount = "SYSTEM"

    if ([string]::IsNullOrWhiteSpace($agent.destinationDir)) {
        Error "Destination dir parameter cannot be empty" 1
    }

    if ([string]::IsNullOrWhiteSpace($agent.account)) {
        $agent.account = $systemAccount
        Message "Account parameter was empty. Changing to [$($agent.account)]"
    }

    if ([string]::IsNullOrWhiteSpace($agent.password)) {
        if ($agent.account -notlike $systemAccount) {
            Error "Password cannot be empty. Empty is allowed only for [$systemAccount]" 2
        }
    }

    Message "Script parameter are checked"
}

function CheckIfScriptVersionIsNewer() {
    Message "Checking script version"
    Message "-------------------- NOT IMPLEMENTED --------------------"
    $isNewerVersion = $true

    if (!$isNewerVersion) {
        Error "Installed version is equal or newer than script's version" 3
    }
    Message "Installed version is older - update. Checking script version - finished"
    return $isNewerVersion
}

function MoveScriptToDirectory() {
    Message "Moving script files from [$($agent.scriptDir)] to destination [$($agent.destinationDir)]"
    if (!(Test-Path $agent.destinationDir)) {
        Message "Creating destination directory [$($agent.destinationDir)]"
        New-Item $agent.destinationDir -ItemType Directory
    }
    $exclude =  '.\git'
    Get-ChildItem $agent.scriptDir -Recurse | where { $_.Name -notlike $exclude } | Select Name, FullName | % {
            $newPath = Join-Path $agent.destinationDir $_.FullName.Substring($agent.scriptDir.length);
            if (!(((Get-ChildItem $_.FullName).PSIsContainer) -and (Test-Path $newPath))) {
                Copy-Item -Path $_.FullName -Destination $newPath -Force -Verbose
            }
        }
    Message "Moving script files to destination - finished"
}

function RemoveScriptInDestination() {
    Message "Remove script in destination [$($agent.destinationDir)]"
    if (Test-Path $agent.destinationDir) {
        Remove-Item $agent.destinationDir -Recurse -Force -Verbose
    } else {
        Message "Cannot access destination dir - it is removed or no permission" "YELLOW"
    }
    Message "Remove script in destination [$($agent.destinationDir)] - finished"
}

function UnregisterScheduledTask() {
    Message "Unregistering scheduled task"
    $task = Get-Scheduledtask $agent.taskName -ErrorAction SilentlyContinue
    if ($task) {
        Message "Scheduled task exists - unregistering"
        Unregister-ScheduledTask $agent.TaskName -Confirm:$false
        Message "Scheduled task exists unregistered. Removing task folder"
        $scheduleObject = New-Object -ComObject schedule.service
        $scheduleObject.connect()
        $rootFolder = $scheduleObject.GetFolder("\")
        $rootFolder.DeleteFolder($agent.taskPath,$null)
        Message "Task folder removed"
    } else {
        Message "Scheduled task does not exists - nothing to unregister" "YELLOW"
    }
    Message "Unregistering scheduled task - finished"
}

function CreateScheduledTask() {
    Message "Creating scheduled task for Simple Monitor Agent"
    $runScriptPath = Join-Path $agent.scriptDir "runSimpleMonitorAgent.ps1"
    $profilePath = Join-Path $agent.scriptDir $agent.profileFile
    $action = New-ScheduledTaskAction -Execute "Powershell.exe" -Argument ("-NoProfile -ExecutionPolicy Bypass -File `"{0}`" -agentProfilePath `"{1}`"" -f $runScriptPath, $profilePath)
    $trigger = New-ScheduledTaskTrigger -Once -At (Get-Date) -RepetitionInterval (New-TimeSpan -Minutes 1) -RepetitionDuration (New-TimeSpan -Days 365)
    $settings = New-ScheduledTaskSettingsSet -Hidden -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -StartWhenAvailable
    if ([string]::IsNullOrWhiteSpace($agent.password)) {
        $principal = New-ScheduledTaskPrincipal -UserId $agent.account -LogonType ServiceAccount -RunLevel Highest
        $task = New-ScheduledTask -Action $action -Trigger $trigger -Settings $settings -Principal $principal
        Register-ScheduledTask ($agent.taskName) -InputObject $task -TaskPath $agent.taskPath
    } else {
        $task = New-ScheduledTask -Action $action -Trigger $trigger -Settings $settings
        Register-ScheduledTask ($agent.taskName) -InputObject $task -User ($agent.account) -Password $agent.password -TaskPath $agent.taskPath
    }

    Message "Creating scheduled task for Simple Monitor Agent - finished"
}

function RunJobs($jobs) {
    Message "Running jobs: $([string]::Join(", ", $jobs))"
    foreach ($job in $jobs) {
        &$job
    }
    Message "All jobs finished"
}

function Run() {
    Message "Agent installation is running"
    CheckParams

    $installJobs = @(
        "CheckIfScriptVersionIsNewer",
        "UnregisterScheduledTask",
        "CreateScheduledTask"
    )
    $uninstallJobs = @("UnregisterScheduledTask")

    if ($uninstall.IsPresent) {
        Message "Performing uninstall of SimpleMonitorAgent"
        RunJobs $uninstallJobs
    } else {
        Message "Performing install of SimpleMonitorAgent"
        RunJobs $installJobs
    }

    Message "Agent installation is finished"
}

Run